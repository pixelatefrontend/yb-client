import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import IconSprite from './views/IconSprite'
import Home from './views/Home'
import Dorms from './views/Dorms'
import DormDetail from './views/DormDetail'
import DormSearch from './views/DormSearch'
import CityList from './views/CityList'
import City from './views/City/City'
import CityMain from './views/City/CityMain'
import CityGender from './views/City/CityGender'
import CityDistrict from './views/City/CityDistrict'
import CityDistrictGender from './views/City/CityDistrictGender'
import University from './views/University/University'
import UniversityMain from './views/University/UniversityMain'
import UniversityGender from './views/University/UniversityGender'
import UniversityFaculty from './views/University/UniversityFaculty'
import UniversityFacultyGender from './views/University/UniversityFacultyGender'
import UniSearch from './views/UniSearch'
import UniDetail from './views/UniDetail'
import UniDetailFaculty from './views/UniDetailFaculty'
import UniDetailAbout from './views/UniDetailAbout'
import UniDetailCampus from './views/UniDetailCampus'
import UniQuestionsAnswer from './views/UniQuestionsAnswer'
import Blog from './views/Blog'
import BlogDetail from './views/BlogDetail'
import BlogCategory from './views/BlogCategory'
import CityDetail from './views/CityDetail/CityDetail'
import CityDetailAbout from './views/CityDetail/CityDetailAbout'
import CityDetailDistrict from './views/CityDetail/CityDetailDistrict'
import CityDetailUnivercities from './views/CityDetail/CityDetailUnivercities'
import About from './views/About'
import Notice from './views/Notice'
import Login from './views/Login'
import Register from './views/Register'
import User from './views/User'
import UserComment from './views/UserComment'
import UserSettings from './views/UserSettings'
import UserOffers from './views/UserOffers'
import UserFavorites from './views/UserFavorites'
import DormUser from './views/DormUser'
import DormUserAccount from './views/DormUserAccount'
import DormUserMessages from './views/DormUserMessages'
import DormUserMessageDetail from './views/DormUserMessageDetail'
import DormUserDorms from './views/DormUserDorms'
import DormUserDormDetail from './views/DormUserDormDetail'
import DormUserDormUpdate from './views/DormUserDormUpdate'
import DormUserStatistic from './views/DormUserStatistic'
import DormUserComments from './views/DormUserComments'
import Faq from './views/Faq'
import Contact from './views/Contact'
import Advertisement from './views/Advertisement'
import UserContract from './views/UserContract'
import SalesContract from './views/SalesContract'
import CancelPolicy from './views/CancelPolicy'
// import RegisterDorm from './views/RegisterDorm'
import ForgetPassword from './views/ForgetPassword'
import PrivacyPolicy from './views/PrivacyPolicy'
import LoginDorm from './views/LoginDorm'
import Media from './views/Media'
import CitySearchResult from './views/CitySearchResult'
import ErrorPage404 from './views/ErrorPage404'
// import SmartMap from './views/SmartMap'
import Branch from './views/Branch/Branch'
import BranchMain from './views/Branch/BranchMain'
// import BranchGender from './views/Branch/BranchGender'
// import BranchDistrict from './views/Branch/BranchDistrict'
// import BranchDistrictGender from './views/Branch/BranchDistrictGender'
import RequestPrice from './views/RequestPrice'
import RoomList from './views/RoomList'
import Room from './views/Room/Room'
import RoomMain from './views/Room/RoomMain'

// import VueBreadcrumbs from 'vue-breadcrumbs'

// Vue.use(VueBreadcrumbs)
Vue.use(Router)
Vue.use(Meta)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { pageClass: 'home', breadcrumb: 'Yurtlarburada' }
    },
    {
      path: '/icon-sprite',
      name: 'IconSprite',
      component: IconSprite
    },
    {
      path: '/yurtlar',
      name: 'Dorms',
      component: Dorms,
      meta: { pageClass: 'subpage subpage-gray dorm', breadcrumb: 'Yurtlar' }
    },
    {
      path: '/sehirler',
      name: 'CityList',
      component: CityList,
      meta: { pageClass: 'subpage subpage-gray city-list', breadcrumb: 'Şehirler' }
    },
    {
      path: '/odalar',
      name: 'RoomList',
      component: RoomList,
      meta: { pageClass: 'subpage subpage-gray city-list', breadcrumb: 'Odalar' }
    },
    {
      path: '/:slug-yakin-ogrenci-yurtlari/',
      // name: 'University',
      component: University,
      meta: { pageClass: 'subpage city' },
      children: [
        {
          path: '',
          name: 'University',
          component: UniversityMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Yakın Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: ':page',
          name: 'UniversityPage',
          component: UniversityMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Üniversite' }
            ]
          }
        },
        {
          path: '/:slug--:faculty-fakultesi-ogrenci-yurtlari/',
          name: 'UniversityFaculty',
          component: UniversityFaculty,
          meta: { pageClass: 'subpage city', breadcrumb: 'Üniversitesi Öğrenci Yurtları' }
        },
        {
          path: '/:slug--:faculty--:type-ogrenci-yurtlari/',
          name: 'UniversityFacultyGender',
          component: UniversityFacultyGender,
          meta: { pageClass: 'subpage city', breadcrumb: 'Erkek Öğrenci Yurtları' }
        },
        {
          path: '/:slug-yakin-:type-ogrenci-yurtlari/',
          name: 'UniversityGender',
          component: UniversityGender,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Yakın Erkek Öğrenci Yurtları' }
            ]
          }
        }
      ]
    },
    {
      path: '/:city-ozel-ogrenci-yurtlari/',
      // name: 'City',
      component: City,
      meta: { pageClass: 'subpage city' },
      children: [
        {
          path: '',
          name: 'City',
          component: CityMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Özel Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: ':page',
          name: 'CityPage',
          component: CityMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'CityPage' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-ogrenci-yurtlari/',
          name: 'CityDistrict',
          component: CityDistrict,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-ogrenci-yurtlari/:page',
          name: 'CityDistrictPage',
          component: CityDistrict,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-:type-ogrenci-yurtlari/',
          name: 'CityDistrictGender',
          component: CityDistrictGender,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Erkek Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:type-ogrenci-yurtlari/',
          name: 'CityGender',
          component: CityGender,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Erkek Öğrenci Yurtları' }
            ]
          }
        }
      ]
    },
    {
      path: '/:city-ogrenci-odalari/',
      // name: 'City',
      component: Room,
      meta: { pageClass: 'subpage city' },
      children: [
        {
          path: '',
          name: 'Room',
          component: RoomMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Öğrenci Odaları' }
            ]
          }
        },
        {
          path: ':page',
          name: 'RoomPage',
          component: RoomMain,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'CityPage' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-odalari/',
          name: 'RoomDistrict',
          component: CityDistrict,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-odalari/:page',
          name: 'RoomDistrictPage',
          component: CityDistrict,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:district-ilcesi-:type-odalari/',
          name: 'RoomDistrictGender',
          component: CityDistrictGender,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'İlçesi Erkek Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: '/:city-:type-odalari/',
          name: 'RoomGender',
          component: CityGender,
          meta: {
            pageClass: 'subpage city',
            breadcrumb: [
              { name: 'Erkek Öğrenci Yurtları' }
            ]
          }
        }
      ]
    },
    {
      path: '/universiteler',
      name: 'UniSearch',
      component: UniSearch,
      meta: { pageClass: 'subpage subpage-gray uni-detail uni-search', breadcrumb: 'Üniversiteler' }
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog,
      meta: {
        pageClass: 'subpage subpage-gray blog',
        breadcrumb: [
          { name: 'Blog' }
        ]
      }
    },
    {
      path: '/blog/:slug--:id',
      name: 'BlogDetail',
      component: BlogDetail,
      meta: {
        pageClass: 'subpage subpage-gray blog',
        breadcrumb: [
          { name: 'Blog' }
        ]
      }
    },
    {
      path: '/blog/kategori/:slug',
      name: 'BlogCategory',
      component: BlogCategory,
      meta: {
        pageClass: 'subpage-gray blog',
        breadcrumb: [
          { name: 'Blog Kategori' }
        ]
      }
    },
    {
      path: '/:city-sehri-hakkinda/',
      component: CityDetail,
      meta: { pageClass: 'subpage uni-detail', disableLoader: true },
      children: [
        {
          path: '',
          name: 'CityDetailAbout',
          component: CityDetailAbout,
          meta: {
            pageClass: 'subpage uni-detail',
            disableLoader: true
          }
        },
        {
          path: 'ilceler',
          name: 'CityDetailDistrict',
          component: CityDetailDistrict,
          meta: { pageClass: 'subpage uni-detail district', disableLoader: true }
        },
        {
          path: 'universiteler',
          name: 'CityDetailUnivercities',
          component: CityDetailUnivercities,
          meta: { pageClass: 'subpage uni-detail', disableLoader: true }
        }
      ]
    },
    {
      path: '/:slug-hakkinda',
      component: UniDetail,
      meta: { pageClass: 'subpage uni-detail' },
      children: [
        {
          path: '',
          name: 'UniDetailAbout',
          component: UniDetailAbout,
          meta: {
            pageClass: 'subpage uni-detail',
            disableLoader: true,
            breadcrumb: [
              { name: 'Üniversitesi Hakkında' }
            ]
          }
        },
        {
          path: 'kampus',
          name: 'UniDetailCampus',
          component: UniDetailCampus,
          meta: { pageClass: 'subpage uni-detail', disableLoader: true }
        },
        {
          path: 'fakulteler',
          name: 'UniDetailFaculty',
          component: UniDetailFaculty,
          meta: { pageClass: 'subpage uni-detail', disableLoader: true }
        },
        {
          path: 'soru-cevap',
          name: 'UniQuestionsAnswer',
          component: UniQuestionsAnswer,
          meta: { pageClass: 'subpage questions-answer', disableLoader: true }
        }
      ]
    },
    {
      path: '/yurtlara-ozel-ilanlar',
      name: 'Notice',
      component: Notice,
      meta: { pageClass: 'subpage uni-detail notice' }
    },
    {
      path: '/hakkimizda',
      name: 'About',
      component: About,
      meta: { pageClass: 'subpage-gray about' }
    },
    {
      path: '/basinda-biz',
      name: 'Media',
      component: Media,
      meta: { pageClass: 'subpage-gray about medias' }
    },
    {
      path: '/uye-girisi',
      name: 'Login',
      component: Login,
      meta: { pageClass: 'subpage subpage-gray login' }
    },
    {
      path: '/yurt-girisi',
      name: 'LoginDorm',
      component: LoginDorm,
      meta: { pageClass: 'subpage subpage-gray login' }
    },
    {
      path: '/uye-ol',
      name: 'Register',
      component: Register,
      meta: { pageClass: 'subpage subpage-gray login register' }
    },
    {
      path: '/kullanici-yonetimi',
      component: User,
      meta: { pageClass: 'subpage subpage-gray user' },
      children: [
        {
          path: '',
          name: 'UserSettings',
          component: UserSettings,
          meta: { pageClass: 'subpage subpage-gray user' }
        },
        {
          path: 'yorumlarim',
          name: 'UserComment',
          component: UserComment,
          meta: { pageClass: 'subpage subpage-gray user' }
        },
        {
          path: 'teklifler',
          name: 'UserOffers',
          component: UserOffers,
          meta: { pageClass: 'subpage subpage-gray user' }
        },
        {
          path: 'favorilerim',
          name: 'UserFavorites',
          component: UserFavorites,
          meta: { pageClass: 'subpage subpage-gray user' }
        }
      ]
    },
    {
      path: '/yurt-yonetimi',
      component: DormUser,
      meta: { pageClass: 'subpage subpage-gray user' },
      children: [
        {
          path: 'hesap-bilgileri',
          name: 'DormUserAccount',
          component: DormUserAccount,
          meta: { pageClass: 'subpage uni-detail subpage-gray user' }
        },
        {
          path: 'mesajlar',
          name: 'DormUserMessages',
          component: DormUserMessages,
          meta: { pageClass: 'subpage subpage-gray user' }
        },
        {
          path: 'mesajlar/:id',
          name: 'DormUserMessageDetail',
          component: DormUserMessageDetail,
          meta: { pageClass: 'subpage subpage-gray user' }
        },
        {
          path: 'yurtlar',
          name: 'DormUserDorms',
          component: DormUserDorms,
          meta: { pageClass: 'subpage subpage-gray user my-dorm' }
        },
        // {
        //   path: 'yurtlar/:id',
        //   name: 'DormUserDormDetail',
        //   component: DormUserDormDetail,
        //   meta: { pageClass: 'subpage subpage-gray user' }
        // },
        {
          path: 'istatistikler',
          name: 'DormUserStatistic',
          component: DormUserStatistic,
          meta: { pageClass: 'subpage uni-detail subpage-gray user' }
        },
        {
          path: 'yorumlar',
          name: 'DormUserComments',
          component: DormUserComments,
          meta: { pageClass: 'subpage subpage-gray user uni-detail' }
        },
        {
          path: 'ekle',
          name: 'DormUserDormDetail',
          component: DormUserDormDetail,
          meta: {
            pageClass: 'subpage subpage-gray user account-dorm'
          }
        },
        {
          path: 'duzenle',
          name: 'DormUserDormUpdate',
          component: DormUserDormUpdate,
          meta: {
            pageClass: 'subpage subpage-gray user account-dorm'
          }
        }
      ]
    },
    {
      path: '/sik-sorulan-sorular',
      name: 'Faq',
      component: Faq,
      meta: {
        pageClass: 'subpage subpage-gray questions',
        breadcrumb: [
          { name: 'Sık Sorulan Sorular' }
        ]
      }
    },
    {
      path: '/iletisim',
      name: 'Contact',
      component: Contact,
      meta: {
        pageClass: 'subpage subpage-gray contact',
        breadcrumb: [
          { name: 'İletişim' }
        ]
      }
    },
    {
      path: '/yurt-arama',
      name: 'DormSearch',
      component: DormSearch,
      meta: {
        pageClass: 'subpage uni-detail subpage-gray dorm-search',
        breadcrumb: [
          { name: 'Öğrenci Yurtları', link: 'CityPage' }
        ]
      }
    },
    {
      path: '/akilli-harita',
      name: 'DormSearch_akilli-harita',
      component: DormSearch,
      meta: {
        pageClass: 'subpage uni-detail subpage-gray dorm-search',
        breadcrumb: [
          { name: 'Akıllı Harita' }
        ]
      }
    },
    {
      path: '/yurt-fiyatlari',
      name: 'RequestPrice',
      component: RequestPrice,
      meta: {
        pageClass: 'subpage uni-detail subpage-gray dorm-search request-price',
        breadcrumb: [
          { name: 'Akıllı Harita' }
        ]
      }
    },
    {
      path: '/yurdumu-ekle',
      name: 'Advertisement',
      component: Advertisement,
      meta: {
        pageClass: 'subpage subpage-gray advertisement',
        breadcrumb: [
          { name: 'Yurdumu Ekle' }
        ]
      }
    },
    {
      path: '/kullanici-sozlesmesi',
      name: 'UserContract',
      component: UserContract,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/mesafeli-satis-sozlesmesi',
      name: 'SalesContract',
      component: SalesContract,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/iptal-ve-iade-kosullari',
      name: 'CancelPolicy',
      component: CancelPolicy,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/gizlilik-politikasi',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/sifremi-unuttum',
      name: 'ForgetPassword',
      component: ForgetPassword,
      meta: { pageClass: 'subpage uni-detail subpage-gray forget' }
    },
    /*     {
      path: '/yurt-kayit',
      name: 'RegisterDorm',
      component: RegisterDorm,
      meta: { pageClass: 'subpage uni-detail subpage-gray register' }
    }, */
    {
      path: '/arama/:type--:slug--:id',
      name: 'CitySearchResult',
      component: CitySearchResult,
      meta: { pageClass: 'subpage subpage-gray' }
    },
    {
      path: '/:slug-y-:id',
      name: 'DormDetail',
      component: DormDetail,
      meta: { pageClass: 'subpage subpage-gray dorm detail-dorm' }
    },
    {
      path: '/Yurt/:city/:slug',
      name: 'DormDetailRedirect',
      props: { redirect: true },
      component: DormDetail,
      meta: { pageClass: 'subpage subpage-gray dorm detail-dorm' }
    },
    {
      path: '/:dorm-s-:id/',
      // path: '/subeler',
      props: { redirect: false },
      component: Branch,
      meta: { pageClass: 'subpage subpage-gray dorm detail-dorm' },
      children: [
        {
          path: '',
          name: 'BranchMain',
          component: BranchMain,
          meta: {
            pageClass: 'subpage city specialBg',
            breadcrumb: [
              { name: 'Yakın Öğrenci Yurtları' }
            ]
          }
        },
        {
          path: ':page',
          name: 'BranchPage',
          component: BranchMain,
          meta: {
            pageClass: 'subpage city specialBg'
          }
        }
        /*         {
      path: '/:dorm-:type-ogrenci-yurtlari/',
      name: 'BranchGender',
      component: BranchGender,
      meta: {
        pageClass: 'subpage city',
        breadcrumb: [
          { name: 'Erkek Öğrenci Yurtları' }
        ]
      }
    },
    {
      path: '/:dorm-:district-ilcesi-ogrenci-yurtlari/',
      name: 'BranchDistrict',
      component: BranchDistrict,
      meta: {
        pageClass: 'subpage city',
        breadcrumb: [
          { name: 'Erkek Öğrenci Yurtları' }
        ]
      }
    },
    {
      path: '/:dorm--:district--:type-ogrenci-yurtlari/',
      name: 'BranchDistrictGender',
      component: BranchDistrictGender,
      meta: { pageClass: 'subpage city', breadcrumb: 'Erkek Öğrenci Yurtları' }
    },     */

      ] },
    /*
    404 sayfasının altına herhangi bir şekilde herhangi bir koşulda
    herhangi bir niyet ile yeni bir router eklemeyin.
    Router dosyası "abi 404 router'ımın altına yeni router eklesene" dese
    bile eklemeyin.
    Hz. İsa dirilip "İnsanlığın kurtuluşu 404 sayfasının altına
    yeni router eklemeye bağlı" dese bile eklemeyin.
    Allah'ını seven 404 router'ının altına yeni router eklemesin.
    */
    {
      path: '*',
      name: 'ErrorPage404',
      component: ErrorPage404,
      meta: { pageClass: 'subpage subpage-gray' }
    }
  ]
})
