/* eslint-disable camelcase */
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App.vue'
import router from './router'
import store from './store/'
// eslint-disable-next-line no-unused-vars
import { loggedIn, getToken } from './common/auth'
import { SET_USER_INFO } from '@/store/mutations.type'
import './registerServiceWorker'
import * as VueGoogleMaps from 'vue2-google-maps'
import SvgIcon from './components/SvgIcon'
import VueButton from './components/VueButton'
import VueLink from './components/VueLink'
import Container from './components/Container'
import Row from './components/Row'
import SlideUpDown from './components/SlideUpDown'
import VueInput from './components/VueInput'
import VueSelect from './components/VueSelect'
import SelectBox from './components/SelectBox'
import Modal from './components/Modal'
import EmptyContent from './components/EmptyContent'
import ComponentDataLoader from './components/ComponentDataLoader'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import helpers from './helpers.js'

import {
  LOGOUT,
  GET_PAGE_CLASS,
  SHOW_PRELOADER,
  SHOW_MENU
} from '@/store/actions.type'

const BASE_API_URL = process.env.VUE_APP_API_URL

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = BASE_API_URL
Vue.axios.defaults.proxyHeaders = false
Vue.axios.defaults.credentials = false
Vue.axios.defaults.headers.common['Accept'] = 'application/json'
Vue.axios.defaults.headers.common['Content-Type'] = 'application/json'

Vue.use(VueAwesomeSwiper, {})

Vue.config.productionTip = false
Vue.config.performance = process.env.NODE_ENV !== 'production'
Vue.config.devtools = process.env.NODE_ENV !== 'production'
Vue.filter('truncate', function (value, limit) {
  if (value.length > limit) {
    value = value.substring(0, (limit - 3)) + '...'
  }

  return value
})
Vue.component('svg-icon', SvgIcon)
Vue.component('v-button', VueButton)
Vue.component('v-link', VueLink)
Vue.component('container', Container)
Vue.component('row', Row)
Vue.component('slide-up-down', SlideUpDown)
Vue.component('v-input', VueInput)
Vue.component('v-select', VueSelect)
Vue.component('component-data-loader', ComponentDataLoader)
Vue.component('modal', Modal)
Vue.component('select-box', SelectBox)
Vue.component('empty-content', EmptyContent)

Vue.axios.interceptors.response.use(undefined, (err) => {
  return new Promise((resolve, reject) => {
    // Not needed this condition but maybe this will be required in the future.
    // if (err.response.status === 400 && err.response.data.error === 'token_not_provided') {
    //   console.log('token not provided')
    //   store.dispatch(LOGOUT)
    //   router.push({ name: 'login' })
    // }

    // This condition will be true for every request made after token expired.
    // This means LOGOUT action will be dispatched multiple times.
    // And router will redirect to login page a few times.
    // This may harmless but to bu sure, careful observation required.
    // Because of redirecting to login page multiple times,
    // It will push redirect parameters to url address.
    // We want this to be happen.
    // Disabled temporarily.
    // If there is request for redirecting user to the page where he/she left
    // we should reconsider this code.(router.beforeEach)
    if (err.response && err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
      window.localStorage.setItem('isLogin', '')
      store.dispatch(LOGOUT)
      router.push({ name: 'Login' })
    }
    throw err
  })
})

const mytoken = window.localStorage.getItem('access_token')
const first_name = window.localStorage.getItem('first_name1')
const last_name = window.localStorage.getItem('last_name1')
const isLogin = window.localStorage.getItem('isLogin')
const email = window.localStorage.getItem('mail1')
const phone = window.localStorage.getItem('phone1')
const university_name = window.localStorage.getItem('university_name1')
const faculty_name = window.localStorage.getItem('faculty_name1')
const university_id = window.localStorage.getItem('university_id1')
const faculty_id = window.localStorage.getItem('faculty_id1')

let user = {
  first_name: first_name,
  last_name: last_name,
  email: email,
  phone: phone,
  university_name: university_name,
  faculty_name: faculty_name,
  university_id: university_id,
  faculty_id: faculty_id
}
// user.isLogin = isLoginwindow.localStorage.getItem('university_name1')
// When user refresh the web page
if (mytoken) {
  let data = { updateLogin: true, isLogin: isLogin }
  store.commit(SET_USER_INFO, data)
  // console.log('local storage: ' + first_name +  ' ' +  last_name + ' ' + isLogin)
  store.commit(SET_USER_INFO, user)
  // (getToken()) {
  // Vue.axios.defaults.params = {}
  // Vue.axios.defaults.params['token'] = getToken()
  Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + mytoken // this.getToken
}

router.beforeEach((to, from, next) => {
  const routerNext = () => {
    setTimeout(() => {
      next()
    }, 300)
  }
  setTimeout(() => {
    store.dispatch(GET_PAGE_CLASS, to.meta.pageClass)
  }, 300)

  if (!to.meta.disableLoader) {
    // eslint-disable-next-line eqeqeq
    if (to.fullPath == '/sehirler' || to.fullPath == '/universiteler' || to.fullPath == '/yurt-arama' || to.fullPath == '/') {
      store.dispatch(SHOW_PRELOADER, true)
      routerNext(next)
    }
  } else {
    store.dispatch(SHOW_PRELOADER, false)
    next()
  }
  store.dispatch(SHOW_MENU, false)

  // If there is no page for the requested path
  // Then return the authenticated user to home page
  // or to login page if the user is not authenticated.
  if (!to.matched.length) {
    if (loggedIn()) {
      next({
        path: '/'
      })
    } else {
      next({
        path: '/uye-girisi'
      })
    }
  }

  // If authenticated user try to go to login or register page
  // redirect him/her to home page.
  if (to.name === 'Login' || to.name === 'Register') {
    if (loggedIn()) {
      next({
        path: from.fullPath
      })
    } else {
      routerNext(next)
    }
  }

  // Do not route an user to protected resources.
  // Redirect to login page instead.
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!loggedIn()) {
      next({
        path: '/uye-girisi'
      })
    } else {
      routerNext(next)
    }
  } else {
    routerNext(next)
  }
})

router.afterEach((to, from) => {
  setTimeout(() => { store.dispatch(SHOW_PRELOADER, false) }, 700)
})

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDEfw312zAAkWgW6pluDxXbsuOKsbgBvBs',
    // key: 'AIzaSyA7KCkWe8OAxuniV0d0AdjerEYI3Qcrfrs',
    libraries: 'places'
  }
})

const plugin = {
  install (Vue, options) {
    Vue.prototype.$helpers = helpers // we use $ because it's the Vue convention
  }
}

Vue.use(plugin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
