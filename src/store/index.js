import Vue from 'vue'
import Vuex from 'vuex'
import common from '@/store/modules/common'
import settings from '@/store/modules/settings'
import user from '@/store/modules/user'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    common,
    settings,
    user
  },
  strict: debug
})
