import Vue from 'vue'
import * as userApi from '@/api/user'

import { LOGIN, LOGOUT, GET_USER } from '@/store/actions.type'
import { SET_TOKEN, UNSET_TOKEN, SET_USER_INFO } from '@/store/mutations.type'

import { getToken, setToken, removeToken } from '@/common/auth'

const state = {
  token: '',
  orders: {},
  addresses: [],
  user: {},
  first_name: '',
  last_name: '',
  dorm_name: '',
  university_id: '',
  faculty_id: '',
  university_name: '',
  faculty_name: '',
  phone: '',
  email: '',
  mail1: '',
  isLogin: '',
  type_id: ''
}

const mutations = {
  [SET_TOKEN] (state, token) {
    state.token = token
  },
  [UNSET_TOKEN] (state) {
    state.token = ''
  },
  [SET_USER_INFO] (state, data) {
    if (data) {
      if (data.updateLogin) {
        state.isLogin = data.isLogin
      } else {
        // state.user = data
        state.first_name = data.first_name
        state.last_name = data.last_name
        if (data.dorm_name) {
          state.dorm_name = data.dorm_name.slice(0, 10) + '...'
        }

        state.university_id = data.university_id
        state.faculty_name = data.faculty_name
        state.university_name = data.university_name
        state.faculty_id = data.faculty_id
        state.phone = data.phone
        state.mail1 = data.email
        state.type_id = data.type_id
      // state.isLogin = data.isLogin
      }
    }
  }
}

const actions = {
  [LOGIN] ({ commit }, { email, password }) {
    return userApi.login(email, password)
      .then(({ data }) => {
        Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.token
        commit(SET_TOKEN, data.token)
        setToken(data.token)
      })
      .catch((error) => {
        console.log('An error occurred during login process. Here is the reason: ', error)
        throw new Error(error)
      })
  },

  [LOGOUT] ({ commit }) {
    return userApi.logout()
      .then(({ data }) => {
        delete Vue.axios.defaults.headers.common['Authorization']
        commit(UNSET_TOKEN)
        removeToken()
      })
  },

  [GET_USER] ({ commit }) {
    return userApi.profile()
      .then(({ data }) => {
        commit(SET_USER_INFO, data)
      })
  }
}

const getters = {
/*   token (state) { return getToken() },
  isAuthenticated (state) { return state.token || getToken() }, */
  // user: state => state.user,
  first_name: state => state.first_name,
  last_name: state => state.last_name,
  dorm_name: state => state.dorm_name,
  university_id: state => state.university_id,
  faculty_id: state => state.faculty_id,
  university_name: state => state.university_name,
  faculty_name: state => state.faculty_name,
  mail1: state => state.mail1,
  phone: state => state.phone,
  isLogin: state => state.isLogin,
  type_id: state => state.type_id
}

export default {
  state,
  mutations,
  actions,
  getters
}
