import {
  SET_PAGE_CLASS,
  SET_PRELOADER,
  SET_CITY_DETAILS,
  SET_SHOWMENU,
  SET_UNIVERSITY_DETAILS
} from '@/store/mutations.type'

import {
  GET_PAGE_CLASS,
  SHOW_PRELOADER,
  GET_CITY_DETAILS,
  SHOW_MENU,
  GET_UNIVERSITY_DETAILS
} from '@/store/actions.type'

const state = {
  pageClass: '',
  showPreloader: false,
  showMenu: false,
  cityDetails: null,
  universityDetails: null,
  user: {}
}

const mutations = {
  [SET_PAGE_CLASS] (state, status) {
    state.pageClass = status
  },
  [SET_PRELOADER] (state, status) {
    state.showPreloader = status
  },
  [SET_SHOWMENU] (state, status) {
    state.showMenu = status
  },
  [SET_CITY_DETAILS] (state, status) {
    state.cityDetails = status
  },
  [SET_UNIVERSITY_DETAILS] (state, status) {
    state.universityDetails = status
  }
}

const actions = {
  [GET_PAGE_CLASS] ({ commit }, status) {
    commit(SET_PAGE_CLASS, status)
  },
  [SHOW_PRELOADER] ({ commit }, status) {
    commit(SET_PRELOADER, status)
  },
  [SHOW_MENU] ({ commit }, status) {
    commit(SET_SHOWMENU, status)
  },
  [GET_CITY_DETAILS] ({ commit }, status) {
    commit(SET_CITY_DETAILS, status)
  },
  [GET_UNIVERSITY_DETAILS] ({ commit }, status) {
    commit(SET_UNIVERSITY_DETAILS, status)
  }
}

const getters = {
  pageClass (state) { return state.pageClass },
  showPreloader (state) { return state.showPreloader },
  cityDetails (state) { return state.cityDetails },
  showMenu (state) { return state.showMenu },
  universityDetails (state) { return state.universityDetails },
  user (state) { return state.user }
}


export default {
  state,
  mutations,
  actions,
  getters
}
