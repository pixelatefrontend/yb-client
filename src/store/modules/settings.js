import * as settings from '@/api/settings'

import {
  SET_GENERAL_SETTINGS, SET_MOBILE_MENU_ACTIVE, SET_BLOG_CATEGORY_TITLE, SET_BLOG_CATEGORY_SLUG
} from '@/store/mutations.type'

import {
  GET_GENERAL_SETTINGS
} from '@/store/actions.type'

const state = {
  settings: null,
  isMenuActive: true,
  blogCategory: '',
  blogSlug: ''
}

const mutations = {
  [SET_GENERAL_SETTINGS] (state, status) {
    state.settings = status
  },
  [SET_MOBILE_MENU_ACTIVE] (state, status) {
    state.isMenuActive = status
  },
  [SET_BLOG_CATEGORY_TITLE] (state, status) {
    state.blogCategory = status
  },
  [SET_BLOG_CATEGORY_SLUG] (state, status) {
    state.blogSlug = status
  }  
}

const actions = {
  [GET_GENERAL_SETTINGS] ({ commit }) {
    settings.settings()
      .then(({ data }) => {
        commit(SET_GENERAL_SETTINGS, data)
      })
  }
}

const getters = {
  settings: state => state.settings,
  isMenuActive: state => state.isMenuActive,
  blogCategory: state => state.blogCategory,
  blogSlug: state => state.blogSlug
}

export default {
  state,
  mutations,
  actions,
  getters
}
