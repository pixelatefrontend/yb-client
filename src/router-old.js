import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import IconSprite from './views/IconSprite'
import City from './views/City/City'
import CityList from './views/CityList'
import UniDetail from './views/UniDetail'
import UniDetailFaculty from './views/UniDetailFaculty'
import UniDetailAbout from './views/UniDetailAbout'
import UniDetailCampus from './views/UniDetailCampus'
import UniSearch from './views/UniSearch'
import CityDetail from './views/CityDetail/CityDetail'
import CityDetailAbout from './views/CityDetail/CityDetailAbout'
import CityDetailDistrict from './views/CityDetail/CityDetailDistrict'
import CityDetailUnivercities from './views/CityDetail/CityDetailUnivercities'
import Dorms from './views/Dorms'
import CityDorms from './views/CityDorms'
import QuestionsAnswer from './views/UniQuestionsAnswer'
import Blog from './views/Blog'
import BlogDetail from './views/BlogDetail'
import BlogCategory from './views/BlogCategory'
import BlogCategory2 from './views/BlogCategory2'
import About from './views/About'
import SignIn from './views/Login'
import Register from './views/Register'
import Questions from './views/Questions'
import Advertisement from './views/Advertisement'
import Contact from './views/Contact'
import DormDetail from './views/DormDetail'
import UserFavories from './views/UserFavorites'
import UserOffers from './views/UserOffers'
import UserComment from './views/UserComment'
import UserSettings from './views/UserSettings'
import DormSearch from './views/DormSearch'
import Notice from './views/Notice'
import SearchPersonel from './views/SearchPersonel'
import VehicleNotices from './views/VehicleNotices'
import SearchJobs from './views/SearchJobs'
import SaleDorm from './views/SaleDorm'
import RentBuilding from './views/RentBuilding'
import Request from './views/Request'
import Vehicles from './views/Vehicles'
import SmartMap from './views/SmartMap'
import University from './views/University/University'
import UserContract from './views/UserContract'
import SalesContract from './views/SalesContract'
import CancelPolicy from './views/CancelPolicy'
import PrivacyPolicy from './views/PrivacyPolicy'
import ForgetPassword from './views/ForgetPassword'
import RegisterDorm from './views/RegisterDorm'
import DormAccount from './views/DormUserAccount'
import DormMessageList from './views/DormUserMessages'
import DormMessageDetail from './views/DormUserMessageDetail'
import DormAccountList from './views/DormUserDorms'
import MyDormDetail from './views/DormUserDormDetail'
import DormStatistic from './views/DormUserStatistic'
import MyDormComments from './views/DormUserComments'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior () {
    return { x: 0, y: 0 }
  },
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { pageClass: 'home' }
    },
    {
      path: '/icon-sprite',
      name: 'IconSprite',
      component: IconSprite
    },
    {
      path: '/sehir/:slug--:id',
      name: 'City',
      component: City,
      meta: { pageClass: 'subpage city' }
    },
    {
      path: '/universite-detay',
      name: 'University',
      component: University,
      meta: { pageClass: 'subpage city' }
    },
    {
      path: '/universite/:slug',
      name: 'UniDetail',
      component: UniDetail,
      meta: { pageClass: 'subpage uni-detail' },
      children: [
        {
          path: '',
          name: 'UniDetailAbout',
          component: UniDetailAbout,
          meta: { pageClass: 'subpage uni-detail' }
        },
        {
          path: 'kampus',
          name: 'UniDetailCampus',
          component: UniDetailCampus,
          meta: { pageClass: 'subpage uni-detail' }
        },
        {
          path: 'fakulte',
          name: 'UniDetailFaculty',
          component: UniDetailFaculty,
          meta: { pageClass: 'subpage uni-detail' }
        },
        {
          path: 'soru-cevap',
          name: 'QuestionsAnswer',
          component: QuestionsAnswer,
          meta: { pageClass: 'subpage questions-answer' }
        }
      ]
    },
    {
      path: '/sehir-detay',
      component: CityDetail,
      meta: { pageClass: 'subpage uni-detail' },
      children: [
        {
          path: '',
          name: 'CityDetailAbout',
          component: CityDetailAbout,
          meta: { pageClass: 'subpage uni-detail' }
        },
        {
          path: 'ilceler',
          name: 'CityDetailDistrict',
          component: CityDetailDistrict,
          meta: { pageClass: 'subpage uni-detail district' }
        },
        {
          path: 'universiteler',
          name: 'CityDetailUnivercities',
          component: CityDetailUnivercities,
          meta: { pageClass: 'subpage uni-detail' }
        }
      ]
    },
    {
      path: '/sehirler',
      name: 'CityList',
      component: CityList,
      meta: { pageClass: 'subpage subpage-gray city-list' }
    },
    {
      path: '/blog',
      name: 'Blog',
      component: Blog,
      meta: { pageClass: 'subpage subpage-gray blog' },
      children: [
        {
          path: 'kategori1',
          name: 'BlogCategory',
          component: BlogCategory,
          meta: { pageClass: 'subpage-gray blog' }
        },
        {
          path: 'kategori2',
          name: 'BlogCategory2',
          component: BlogCategory2,
          meta: { pageClass: 'subpage-gray blog' }
        }
      ]
    },
    {
      path: '/blog/:slug--:id',
      name: 'BlogDetail',
      component: BlogDetail,
      meta: { pageClass: 'subpage subpage-gray blog' }
    },
    {
      path: '/hakkimizda',
      name: 'About',
      component: About,
      meta: { pageClass: 'subpage-gray about' }
    },
    {
      path: '/giris',
      name: 'SignIn',
      component: SignIn,
      meta: { pageClass: 'subpage subpage-gray login' }
    },
    {
      path: '/kayit-ol',
      name: 'Register',
      component: Register,
      meta: { pageClass: 'subpage subpage-gray login register' }
    },
    {
      path: '/sik-sorulan-sorular',
      name: 'Questions',
      component: Questions,
      meta: { pageClass: 'subpage subpage-gray questions' }
    },
    {
      path: '/yurdumu-ekle',
      name: 'Advertisement',
      component: Advertisement,
      meta: { pageClass: 'subpage subpage-gray advertisement' }
    },
    {
      path: '/iletisim',
      name: 'Contact',
      component: Contact,
      meta: { pageClass: 'subpage subpage-gray contact' }
    },
    {
      path: '/yurtlar',
      name: 'Dorms',
      component: Dorms,
      meta: { pageClass: 'subpage subpage-gray dorm' }
    },
    {
      path: '/yurt/:slug--:id',
      name: 'DormDetail',
      component: DormDetail,
      meta: { pageClass: 'subpage subpage-gray dorm' }
    },
    {
      path: '/kullanici-favoriler',
      name: 'UserFavories',
      component: UserFavories,
      meta: { pageClass: 'subpage subpage-gray user' }
    },
    {
      path: '/kullanici-teklifler',
      name: 'UserOffers',
      component: UserOffers,
      meta: { pageClass: 'subpage subpage-gray user' }
    },
    {
      path: '/kullanici-yorumlar',
      name: 'UserComment',
      component: UserComment,
      meta: { pageClass: 'subpage subpage-gray user' }
    },
    {
      path: '/kullanici-ayarlar',
      name: 'UserSettings',
      component: UserSettings,
      meta: { pageClass: 'subpage subpage-gray user' }
    },
    {
      path: '/universite-ara',
      name: 'UniSearch',
      component: UniSearch,
      meta: { pageClass: 'subpage subpage-gray uni-detail uni-search' }
    },
    {
      path: '/yurt-arama',
      name: 'DormSearch',
      component: DormSearch,
      meta: { pageClass: 'subpage uni-detail subpage-gray dorm-search' }
    },
    {
      path: '/ilanlar',
      // name: 'Notice',
      component: Notice,
      meta: { pageClass: 'subpage uni-detail notice' },
      children: [
        {
          path: '',
          name: 'Request',
          component: Request,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        // {
        //   path: 'alim-talepleri',
        //   name: 'Request',
        //   component: Request,
        //   meta: { pageClass: 'subpage uni-detail notice' }
        // },
        {
          path: 'eleman-arayanlar',
          name: 'SearchPersonel',
          component: SearchPersonel,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        {
          path: 'arac-gerecler',
          name: 'Vehicles',
          component: Vehicles,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        {
          path: 'arac-ilanlari',
          name: 'VehicleNotices',
          component: VehicleNotices,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        {
          path: 'is-arayanlar',
          name: 'SearchJobs',
          component: SearchJobs,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        {
          path: 'satilik-kiralik-yurtlar',
          name: 'SaleDorm',
          component: SaleDorm,
          meta: { pageClass: 'subpage uni-detail notice' }
        },
        {
          path: 'kiralik-binalar',
          name: 'RentBuilding',
          component: RentBuilding,
          meta: { pageClass: 'subpage uni-detail notice' }
        }
      ]
    },
    {
      path: '/harita-arama',
      name: 'SmartMap',
      component: SmartMap,
      meta: { pageClass: 'subpage uni-detail subpage-gray smart-map' }
    },
    {
      path: '/kullanici-sozlesmesi',
      name: 'UserContract',
      component: UserContract,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/mesafeli-satis-sozlesmesi',
      name: 'SalesContract',
      component: SalesContract,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/iptal-ve-iade-kosullari',
      name: 'CancelPolicy',
      component: CancelPolicy,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/gizlilik-politikasi',
      name: 'PrivacyPolicy',
      component: PrivacyPolicy,
      meta: { pageClass: 'subpage uni-detail subpage-gray user-contract' }
    },
    {
      path: '/sifre-yenile',
      name: 'ForgetPassword',
      component: ForgetPassword,
      meta: { pageClass: 'subpage uni-detail subpage-gray forget' }
    },
    {
      path: '/yurt-kayit',
      name: 'RegisterDorm',
      component: RegisterDorm,
      meta: { pageClass: 'subpage uni-detail subpage-gray register' }
    },
    {
      path: '/yurt-hesap-bilgileri',
      name: 'DormAccount',
      component: DormAccount,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    },
    {
      path: '/mesaj-liste',
      name: 'DormMessageList',
      component: DormMessageList,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    },
    {
      path: '/mesaj-detay',
      name: 'DormMessageDetail',
      component: DormMessageDetail,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    },
    {
      path: '/yurt-sube',
      name: 'DormAccountList',
      component: DormAccountList,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    },
    {
      path: '/yurtlarım-detay',
      name: 'MyDormDetail',
      component: MyDormDetail,
      meta: { pageClass: 'subpage uni-detail subpage-gray user my-dorm' }
    },
    {
      path: '/yurtlar-istatistik',
      name: 'DormStatistic',
      component: DormStatistic,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    },
    {
      path: '/yurtlar-yorumlar',
      name: 'MyDormComments',
      component: MyDormComments,
      meta: { pageClass: 'subpage uni-detail subpage-gray user' }
    }
  ]
}
)
